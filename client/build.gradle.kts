plugins {
	application
}

group = "com.pelp88"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}
dependencies {
	// google guava
	implementation("com.google.guava:guava:+")
	// lanterna
	implementation("com.googlecode.lanterna:lanterna:+")
	// https://mvnrepository.com/artifact/org.json/json
	implementation("org.json:json:+")
	// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core
	implementation("com.fasterxml.jackson.core:jackson-core:+")
	// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind
	implementation("com.fasterxml.jackson.core:jackson-databind:+")
	// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations
	implementation("com.fasterxml.jackson.core:jackson-annotations:+")
	// https://mvnrepository.com/artifact/com.google.inject/guice
	implementation("com.google.inject:guice:+")
	// https://mvnrepository.com/artifact/com.google.inject.extensions/guice-assistedinject
	implementation("com.google.inject.extensions:guice-assistedinject:+")
	// https://mvnrepository.com/artifact/com.google.inject.extensions/guice-multibindings
	implementation("com.google.inject.extensions:guice-multibindings:+")
    // https://mvnrepository.com/artifact/org.projectlombok/lombok
	annotationProcessor("org.projectlombok:lombok:+")
	implementation("org.projectlombok:lombok:+")
	// https://mvnrepository.com/artifact/com.googlecode.lanterna/lanterna
	implementation("com.googlecode.lanterna:lanterna:3.1.1")
	// https://mvnrepository.com/artifact/io.github.openfeign/feign-core
	implementation("io.github.openfeign:feign-core:12.1")
	// https://mvnrepository.com/artifact/io.github.openfeign/feign-okhttp
	implementation("io.github.openfeign:feign-okhttp:12.1")
	// https://mvnrepository.com/artifact/io.github.openfeign/feign-gson
	implementation("io.github.openfeign:feign-gson:12.1")
}

testing {
	suites {
		// Configure the built-in test suite
		val test by
		getting(JvmTestSuite::class) {
			// Use JUnit Jupiter test framework
			useJUnitJupiter("5.8.2")
		}
	}
}

application {
	// Define the main class for the application.
	mainClass.set("mangareaderclient.MangaReaderClientApplication")
}
