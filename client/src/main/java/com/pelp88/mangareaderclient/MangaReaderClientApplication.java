package com.pelp88.mangareaderclient;

import com.google.inject.Guice;
import com.pelp88.mangareaderclient.modules.AppModule;
import com.pelp88.mangareaderclient.ui.GuiProvider;
import com.pelp88.mangareaderclient.ui.windows.impl.HubWindowImpl;
import com.pelp88.mangareaderclient.ui.windows.impl.MangaListWindowImpl;

public class MangaReaderClientApplication {
	public static void main(String[] args) {
		var feignInjector = Guice.createInjector(new AppModule());

		var textGui = feignInjector.getInstance(GuiProvider.class);
		var window = feignInjector.getInstance(HubWindowImpl.class);
		var list = feignInjector.getInstance(MangaListWindowImpl.class);
		textGui.addWindow(list);
		textGui.addWindow(window);
		textGui.switchWindows(window);
	}
}
