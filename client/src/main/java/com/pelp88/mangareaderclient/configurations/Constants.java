package com.pelp88.mangareaderclient.configurations;

import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class Constants {
    @Named("apiUrl")
    public static final String API_BASE_URL = "http://localhost:8081";
}
