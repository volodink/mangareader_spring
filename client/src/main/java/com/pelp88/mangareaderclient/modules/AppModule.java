package com.pelp88.mangareaderclient.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.pelp88.mangareaderclient.configurations.Constants;
import com.pelp88.mangareaderclient.rest.clients.*;
import com.pelp88.mangareaderclient.ui.GuiProvider;
import com.pelp88.mangareaderclient.ui.impl.GuiProviderImpl;
import com.pelp88.mangareaderclient.ui.windows.HubWindow;
import com.pelp88.mangareaderclient.ui.windows.MangaListWindow;
import com.pelp88.mangareaderclient.ui.windows.impl.HubWindowImpl;
import com.pelp88.mangareaderclient.ui.windows.impl.MangaListWindowImpl;
import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;

@Singleton
public class AppModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(MangaClient.class).toInstance(createClient(MangaClient.class));
        bind(ChapterClient.class).toInstance(createClient(ChapterClient.class));
        bind(UserClient.class).toInstance(createClient(UserClient.class));
        bind(FavouriteClient.class).toInstance(createClient(FavouriteClient.class));
        bind(PageClient.class).toInstance(createClient(PageClient.class));
        //
        bind(GuiProvider.class).to(GuiProviderImpl.class);
        //
        bind(HubWindow.class).to(HubWindowImpl.class);
        bind(MangaListWindow.class).to(MangaListWindowImpl.class);
    }

    private <T> T createClient(Class<T> clientClass) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(clientClass, Constants.API_BASE_URL);
    }
}
