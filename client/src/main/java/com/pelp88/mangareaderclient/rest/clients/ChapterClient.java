package com.pelp88.mangareaderclient.rest.clients;

import com.pelp88.mangareaderclient.rest.dto.ChapterRsDto;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface ChapterClient {
    @RequestLine("GET /chapters/{id}")
    ChapterRsDto findById(@Param("id") Long id);

    @RequestLine("GET /chapters")
    List<ChapterRsDto> findAll();
}