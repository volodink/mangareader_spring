package com.pelp88.mangareaderclient.rest.clients;

import com.pelp88.mangareaderclient.rest.dto.FavoritesRsDto;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface FavouriteClient {
    @RequestLine("GET /favourites/{id}")
    FavoritesRsDto findById(@Param("id") Long id);

    @RequestLine("GET /favourites")
    List<FavoritesRsDto> findAll();
}