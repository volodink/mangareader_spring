package com.pelp88.mangareaderclient.rest.clients;

import com.pelp88.mangareaderclient.rest.dto.MangaRsDto;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface MangaClient {
    @RequestLine("GET /mangas/{id}")
    MangaRsDto findById(@Param("id") Long id);

    @RequestLine("GET /mangas")
    List<MangaRsDto> findAll();
}
