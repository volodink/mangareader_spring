package com.pelp88.mangareaderclient.rest.clients;

import com.pelp88.mangareaderclient.rest.dto.PageRsDto;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface PageClient {
    @RequestLine("GET /pages/{id}")
    PageRsDto findById(@Param("id") Long id);

    @RequestLine("GET /pages")
    List<PageRsDto> findAll();
}
