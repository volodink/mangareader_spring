package com.pelp88.mangareaderclient.rest.clients;

import com.pelp88.mangareaderclient.rest.dto.UserRsDto;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface UserClient {
    @RequestLine("GET /users/{id}")
    UserRsDto findById(@Param("id") Long id);

    @RequestLine("GET /users")
    List<UserRsDto> findAll();
}
