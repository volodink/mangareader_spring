package com.pelp88.mangareaderclient.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChapterRsDto implements Serializable {
    private Long id;

    private String title;

    private Integer number;

    private Long mangaId;

    private List<Long> pagesId;
}