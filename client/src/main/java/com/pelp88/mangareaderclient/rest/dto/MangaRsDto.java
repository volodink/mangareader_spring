package com.pelp88.mangareaderclient.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MangaRsDto implements Serializable {
    private Long id;

    private String title;

    private String author;

    private String description;

    private List<Long> chapters;
}