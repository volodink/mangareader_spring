package com.pelp88.mangareaderclient.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageRsDto implements Serializable {
    private Long id;

    private Long chapter;

    private Integer pageNumber;

    private String imageUuid;
}