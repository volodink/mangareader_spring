package com.pelp88.mangareaderclient.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRsDto implements Serializable {
    private Long id;

    private String username;

    private String email;

    private String password;

    private List<Long> favorites;
}