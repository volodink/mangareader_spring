package com.pelp88.mangareaderclient.ui;

import com.googlecode.lanterna.gui2.Window;

public interface GuiProvider {
    void addWindow(Window window);
    void switchWindows(Window window);
}
