package com.pelp88.mangareaderclient.ui.impl;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.pelp88.mangareaderclient.ui.GuiProvider;
import com.pelp88.mangareaderclient.ui.windows.HubWindow;
import lombok.Getter;

import java.io.IOException;

@Singleton
@Getter
public class GuiProviderImpl implements GuiProvider {
    private WindowBasedTextGUI textGui;

    @Inject
    public GuiProviderImpl(Provider<HubWindow> hubWindow) throws IOException, InterruptedException {
        synchronized (this) {
            DefaultTerminalFactory terminalFactory = new DefaultTerminalFactory();
            Screen screen = terminalFactory.createScreen();
            screen.startScreen();
            textGui = new MultiWindowTextGUI(screen);
            textGui.addWindowAndWait(hubWindow.get());
        }
    }

    public void addWindow(Window window) {
        textGui.addWindow(window);
    }

    public void switchWindows(Window window) {
        textGui.setActiveWindow(window);
    }
}
