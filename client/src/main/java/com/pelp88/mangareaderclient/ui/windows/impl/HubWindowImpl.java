package com.pelp88.mangareaderclient.ui.windows.impl;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.googlecode.lanterna.gui2.*;
import com.pelp88.mangareaderclient.ui.GuiProvider;
import com.pelp88.mangareaderclient.ui.windows.HubWindow;
import com.pelp88.mangareaderclient.ui.windows.MangaListWindow;

import java.util.List;

public class HubWindowImpl extends BasicWindow implements HubWindow {
    private GuiProvider guiSingleton;
    private MangaListWindow mangaListWindow;

    @Inject
    public HubWindowImpl(Provider<MangaListWindow> mangaListWindow, Provider<GuiProvider> guiSingleton) {
        super("Hub");
        this.guiSingleton = guiSingleton.get();
        this.mangaListWindow = mangaListWindow.get();

        // Create the panel that will hold the buttons
        Panel buttonPanel = new Panel(new GridLayout(5));
        this.setHints(List.of(Hint.CENTERED));

        // Create the buttons
        Button mangaButton = new Button("Manga", () -> {
            this.guiSingleton.switchWindows(this.mangaListWindow);
        });
        Button chapterButton = new Button("Chapter", () -> {
            // Open the chapter UI
        });
        Button userButton = new Button("User", () -> {
            // Open the user UI
        });
        Button favouritesButton = new Button("Favourites", () -> {
            // Open the favourites UI
        });
        Button pageButton = new Button("Page", () -> {
            // Open the page UI
        });

        // Add the buttons to the panel
        buttonPanel.addComponent(mangaButton);
        buttonPanel.addComponent(chapterButton);
        buttonPanel.addComponent(userButton);
        buttonPanel.addComponent(favouritesButton);
        buttonPanel.addComponent(pageButton);

        // Add the button panel to the window
        setComponent(buttonPanel);
    }
}

