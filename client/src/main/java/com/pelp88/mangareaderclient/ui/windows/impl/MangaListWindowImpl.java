package com.pelp88.mangareaderclient.ui.windows.impl;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.table.*;
import com.pelp88.mangareaderclient.ui.GuiProvider;
import com.pelp88.mangareaderclient.rest.dto.MangaRsDto;
import com.pelp88.mangareaderclient.ui.windows.HubWindow;
import com.pelp88.mangareaderclient.ui.windows.MangaListWindow;

import java.util.ArrayList;
import java.util.List;

public class MangaListWindowImpl extends BasicWindow implements MangaListWindow {
    private GuiProvider guiSingleton;
    private HubWindow hubWindow;

    @Inject
    public MangaListWindowImpl(Provider<HubWindow> hubWindow, Provider<GuiProvider> guiSingleton) {
        super("Manga");
        this.guiSingleton = guiSingleton.get();
        this.hubWindow = hubWindow.get();

        this.setHints(List.of(Hint.CENTERED));

        var mangaList = new ArrayList<MangaRsDto>();
        for (long i = 0; i < 10; i++) {
            var manga = new MangaRsDto();
            manga.setId(i);
            manga.setTitle("test " + i);
            manga.setAuthor("test testov " + i);
            manga.setDescription("testtesttest");
            mangaList.add(manga);
        }

        Table<String> table = new Table<>("Title", "Author", "Description");

        for (var manga : mangaList) {
            table.getTableModel().addRow(manga.getTitle(), manga.getAuthor(), manga.getDescription());
        }

        var scrollBar = new ScrollBar(Direction.VERTICAL);

        Button backButton = new Button("Back", () -> {
            this.guiSingleton.switchWindows(this.hubWindow);
        });


        // Add the scrollable panel to the window
        setComponent(Panels.horizontal(table, scrollBar, backButton));
    }
}