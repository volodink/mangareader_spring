# mangareader_client

MangaReader client

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


Not-so-FAQ:
- ...UrlRef(s) - reference URLs (HATEOAS stuff, in code used like {host_url} + {reference_url})
- imageUuid - static content, every image can be accessed like {image_host_url}/uuid.{jpg, png, etc.}