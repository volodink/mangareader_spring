import 'dart:ffi';

import 'package:mangareader_client/entities/abstract_entity.dart';
import 'package:mangareader_client/entities/page.dart';

import 'manga.dart';

class Chapter extends AbstractEntity{
  String title;
  Long number;
  late Manga manga;
  late List<Page> page;

  Chapter(this.title, this.number, {required urlRef}) : super(urlRef: urlRef);
}