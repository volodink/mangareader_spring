import 'package:mangareader_client/entities/abstract_entity.dart';
import 'package:mangareader_client/entities/manga.dart';
import 'package:mangareader_client/entities/user.dart';

class Favorites extends AbstractEntity {
  late User user;
  late Manga manga;

  Favorites({required urlRef}) : super(urlRef: urlRef);
}