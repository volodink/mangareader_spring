import 'package:mangareader_client/entities/abstract_entity.dart';

import 'chapter.dart';

class Manga extends AbstractEntity{
  String title;
  String author;
  String description;
  late List<Chapter> chapters;

  Manga(this.title, this.author, this.description, {required urlRef}) :
        super(urlRef: urlRef);
}