import 'dart:ffi';

import 'package:mangareader_client/entities/abstract_entity.dart';
import 'package:mangareader_client/entities/chapter.dart';

class Page extends AbstractEntity{
  Long pageNumber;
  String imageUuid;
  late Chapter chapter;

  Page(this.pageNumber, this.imageUuid, {required urlRef}) :
        super(urlRef: urlRef);
}