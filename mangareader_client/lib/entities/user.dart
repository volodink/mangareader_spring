import 'package:mangareader_client/entities/abstract_entity.dart';
import 'package:mangareader_client/entities/manga.dart';

class User extends AbstractEntity {
  String username;
  String email;
  String password;
  late List<Manga> manga;

  User(this.username, this.email, this.password, urlRef) :
        super(urlRef: urlRef);
}