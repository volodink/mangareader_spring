import 'package:flutter/material.dart';
import 'package:mangareader_client/screens/manga_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Manga Reader',
      home: MangaListWidget(),
    );
  }
}
