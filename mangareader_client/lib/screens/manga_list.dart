import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MangaListWidget extends StatefulWidget {
  const MangaListWidget({super.key});

  @override
  _MangaListWidgetState createState() => _MangaListWidgetState();
}

class _MangaListWidgetState extends State<MangaListWidget> {
  List<Manga> _mangas = [];

  @override
  void initState() {
    super.initState();
    //_loadMangas();
  }

  _loadMangas() async {
    final response = await http.get(Uri.parse('http://192.168.0.1/mangas'));
    if (response.statusCode == 200) {
      setState(() {
        _mangas = (json.decode(response.body) as List)
            .map((data) => Manga.fromJson(data))
            .toList();
      });
    } else {
      throw Exception('Failed to load mangas');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: ListView.builder(
      itemCount: _mangas.length,
      itemBuilder: (context, index) {
        final manga = _mangas[index];
        return ListTile(
          leading: Image.network(manga.previewImageUrl),
          title: Text(manga.title),
          subtitle: Text(manga.author),
          onTap: () {
            // Navigate to manga detail page
          },
        );
      },
    ));
  }
}

class Manga {
  final String title;
  final String author;
  final String previewImageUrl;

  Manga({required this.title, required this.author, required this.previewImageUrl});

  factory Manga.fromJson(Map<String, dynamic> json) {
    return Manga(
      title: json['title'],
      author: json['author'],
      previewImageUrl: json['preview_image_url'],
    );
  }
}