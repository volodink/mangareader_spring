package com.pelp88.mangareader.persistence.entities;

import lombok.Data;
import jakarta.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
@Entity
@Table(name = "pages")
public class Page {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne
    @JoinColumn(name = "chapters_id", nullable = false)
    private Chapter chapter;

    private Integer pageNumber;

    private String imageUuid; // we will serve images as static content through another webserver (nginx probably)
}
