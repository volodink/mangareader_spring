package com.pelp88.mangareader.persistence.repositories;

import com.pelp88.mangareader.persistence.entities.User;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestController(path = "users")
@Tag(name = "User Controller")
public interface UserRepository extends JpaRepository<User, Long> {
}
