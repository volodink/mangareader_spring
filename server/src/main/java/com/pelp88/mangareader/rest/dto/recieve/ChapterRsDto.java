package com.pelp88.mangareader.rest.dto.recieve;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.Chapter} entity
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChapterRsDto implements Serializable {
    @Schema(description = "Chapter's ID")
    private Long id;

    @Schema(description = "Chapter's title")
    private String title;

    @Schema(description = "Chapter's number (in manga)")
    private Integer number;

    @Schema(description = "Manga ID")
    private Long mangaId;

    @Schema(description = "Pages of chapter, IDs")
    private List<Long> pagesId;
}