package com.pelp88.mangareader.rest.dto.recieve;

import com.pelp88.mangareader.persistence.entities.Favorite;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * A DTO for the {@link Favorite} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FavoritesRsDto implements Serializable {
    @Schema(description = "Favourites ID")
    private Long id;

    @Schema(description = "User's ID")
    private Long userId;

    @Schema(description = "Manga's ID")
    private Long manga;
}