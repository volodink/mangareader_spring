package com.pelp88.mangareader.rest.dto.recieve;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.Manga} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MangaRsDto implements Serializable {
    @Schema(description = "Manga ID")
    private Long id;

    @Schema(description = "Manga title")
    private String title;

    @Schema(description = "Manga's author")
    private String author;

    @Schema(description = "Manga's description")
    private String description;

    @Schema(description = "Manga chapters")
    private List<Long> chapters;
}