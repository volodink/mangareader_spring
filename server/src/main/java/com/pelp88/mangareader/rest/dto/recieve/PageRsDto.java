package com.pelp88.mangareader.rest.dto.recieve;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.Page} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageRsDto implements Serializable {
    @Schema(description = "Page ID")
    private Long id;

    @Schema(description = "Chapter ID")
    private Long chapter;

    @Schema(description = "Page number")
    private Integer pageNumber;

    @Schema(description = "Page image's UUID")
    private String imageUuid;
}