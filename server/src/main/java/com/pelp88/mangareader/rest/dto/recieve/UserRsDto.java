package com.pelp88.mangareader.rest.dto.recieve;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.User} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRsDto implements Serializable {
    @Schema(description = "User ID")
    private Long id;

    @Schema(description = "Username")
    private String username;

    @Schema(description = "User's email")
    private String email;

    @Schema(description = "User's password (encoded)")
    private String password;

    @Schema(description = "User's favourites")
    private List<Long> favorites;
}