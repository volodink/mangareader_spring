package com.pelp88.mangareader.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.Chapter} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChapterRqDto implements Serializable {
    @NotNull
    @Schema(description = "Chapter's title")
    private String title;

    @NotNull
    @Schema(description = "Quantity of pages")
    private Integer number;

    @NotNull
    @Schema(description = "Manga ID")
    private Long manga;

    @NotNull
    @Schema(description = "Pages of chapter, IDs")
    private List<Long> pages;
}