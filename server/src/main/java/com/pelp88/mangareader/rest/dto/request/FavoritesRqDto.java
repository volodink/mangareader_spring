package com.pelp88.mangareader.rest.dto.request;

import com.pelp88.mangareader.persistence.entities.Favorite;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * A DTO for the {@link Favorite} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FavoritesRqDto implements Serializable {
    @NotNull
    @Schema(description = "User ID")
    private Long userId;

    @NotNull
    @Schema(description = "Manga ID")
    private Long mangaId;
}