package com.pelp88.mangareader.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.Manga} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MangaRqDto implements Serializable {
    @NotNull
    @Schema(description = "Manga title")
    private String title;

    @NotNull
    @Schema(description = "Manga's author")
    private String author;

    @NotNull
    @Schema(description = "Manga's description")
    private String description;

    @NotNull
    @Schema(description = "Manga's chapters IDs")
    private List<Long> chaptersId;
}