package com.pelp88.mangareader.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.Page} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageRqDto implements Serializable {
    @NotNull
    @Schema(description = "Chapter's ID")
    private Long chapterId;

    @NotNull
    @Schema(description = "Page's number")
    private Integer pageNumber;

    @NotNull
    @Schema(description = "Page's image UUID")
    private String imageUuid;
}