package com.pelp88.mangareader.rest.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.pelp88.mangareader.persistence.entities.User} entity
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRqDto implements Serializable {
    @NotNull
    @Schema(description = "Username")
    private String username;

    @NotNull
    @Schema(description = "User's email")
    private String email;

    @NotNull
    @Schema(description = "User's password (encoded)")
    private String password;

    @NotNull
    @Schema(description = "User's favourites manga IDs")
    private List<Long> favoritesId;
}